"""
Utility and helper functions.
"""

import random
import string

from django.db.models import Model


def generate_abbreviation(sender: Model, name: str, max_length: int) -> str:
    """
    Generate abbreviation for sender with `abbreviation` field.

    The generation lasts until we cannot set an abbreviation using initials or
    a `length` long unique random string.
    """

    abbreviation = "".join([part[0] for part in name.split(" ")]).lower()

    while sender.objects.filter(abbreviation__iexact=abbreviation).exists():
        abbreviation = "".join(
            [random.choice(string.ascii_letters) for _ in range(0, max_length)]
        ).lower()

    return abbreviation


def get_name_from_email(email: str) -> str:
    """
    Get member name from email address.

    Since we don't always know the name of a member (without querying APIs), as
    a best effort we uses its email address to guess its name.
    """

    name, _ = email.split("@")
    return " ".join([part.capitalize() for part in name.split(".")])
