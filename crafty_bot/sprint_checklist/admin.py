"""
Register admin components to manage sprint checklist integration.
"""

from django.contrib import messages
from django.contrib.admin import ModelAdmin, register

from crafty_bot.sprint_checklist.models import Cell, Role, SprintChecklist
from crafty_bot.sprint_checklist.tasks import personalize_sprint_checklist_task


@register(Cell)
class CellAdmin(ModelAdmin):
    """
    Administrate Cell model.
    """

    search_fields = ("name",)
    list_display = (
        "name",
        "abbreviation",
        "evaluation_board_id",
        "retrospective_board_id",
        "checklist_template_id",
        "workspace_id",
    )
    readonly_fields = ("evaluation_board_id", "retrospective_board_id", "workspace_id")


@register(Role)
class RoleAdmin(ModelAdmin):
    """
    Administrate Role model.
    """

    search_fields = ("name",)
    list_display = ("name", "abbreviation", "permanent", "subscribe_to_boards")
    list_filter = ("permanent", "subscribe_to_boards")


@register(SprintChecklist)
class SprintChecklistAdmin(ModelAdmin):
    """
    Administrate SprintChecklist model.

    In case of sprint checklist personalization failure, the admin can run a
    personalization on the selected boards.
    """

    actions = ["personalize_checklist"]

    readonly_fields = (
        "board_id",
        "board_name",
        "state",
    )

    search_fields = (
        "member",
        "board_name",
    )

    list_display = (
        "board_name",
        "state",
        "board_id",
        "sprint_id",
        "member",
        "assigned_roles",
        "cell",
    )

    list_filter = (
        "state",
        "sprint_id",
        "cell",
        "roles",
    )

    # pylint: disable=no-self-use
    def assigned_roles(self, obj) -> str:
        """
        List assigned roles as a comma separated list.
        """

        return ", ".join([role.name for role in obj.roles.all()])

    def personalize_checklist(self, request, queryset):
        """
        Run checklist personalization on selected board(s).
        """

        for checklist in queryset:
            # The same personalization should take place under any circumstances
            # to ensure consistency
            personalize_sprint_checklist_task.apply_async(
                (checklist.id, [role.name for role in checklist.roles.all()]),
                retry=True,
            )

        self.message_user(
            request,
            f"Personalization started for {len(queryset)} checklist{'s' if len(queryset) != 1 else ''}",
            messages.SUCCESS,
        )

    personalize_checklist.short_description = "Run immediate personalization"  # type: ignore
