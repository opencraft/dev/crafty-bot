# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2015-2020 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Configuration ###############################################################

.PHONY: static upgrade requirements

.DEFAULT_GOAL := help
HELP_SPACING ?= 30
SHELL ?= /bin/bash
PACKAGE_NAME := crafty_bot
PY := python3
PIP := $(PY) -m pip
PY_MANAGE := $(PY) manage.py
PY_MANAGE_TEST := $(PY) manage.py test --settings=config.settings.test --noinput -v2
PY_MANAGE_TEST_COVERAGE := coverage run manage.py test --settings=config.settings.test --noinput -v2
DOCKER_BUILDKIT ?= 1
DOCKER_REGISTRY := "${CI_REGISTRY}/opencraft/dev/crafty-bot"

# Parameters ##################################################################

# For `test.one` use the rest as arguments and turn them into do-nothing targets
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),test.one manage ci-build-image ci-retag-django-image ci-push-images))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

# Commands ####################################################################

help: ## Display this help message.
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-$(HELP_SPACING)s\033[0m %s\n", $$1, $$2}'

clean: cov.clean ## Remove all temporary files.
	find -name '*.pyc' -delete
	find -name '*~' -delete
	find -name '__pycache__' -type d -delete
	rm -rf \
		.coverage \
		.ipython \
		.mypy_cache \
		.pytest_cache \
		build \
		celerybeat.pid \
		htmlcov \
		staticfiles

install_local_dependencies:  ## Install depenedencies for local development.
	$(PIP) install --pre -r requirements/local.txt

static: clean ## Collect static files for production.
	$(PY_MANAGE) collectstatic --noinput

manage: ## Run a management command.
	$(PY_MANAGE) $(RUN_ARGS)

migrate: clean ## Run migrations.
	$(PY_MANAGE) migrate

migrations.check: clean ## Check for unapplied migrations
	!(($(PY_MANAGE) showmigrations | grep '\[ \]') && printf "\n\033[0;31mERROR: Pending migrations found\033[0m\n\n")

migrations: clean ## Generate migrations.
	$(PY_MANAGE) makemigrations

# CI/CD #######################################################################

ci-build-image:
	docker build \
		-t "${DOCKER_REGISTRY}:$(RUN_ARGS)-$(CI_COMMIT_SHORT_SHA)" -t "${DOCKER_REGISTRY}:$(RUN_ARGS)-latest" \
		-f ./compose/production/$(RUN_ARGS)/Dockerfile .

ci-retag-django-image:
	@for service in $(RUN_ARGS) ; do \
		docker tag "${DOCKER_REGISTRY}:django-$(CI_COMMIT_SHORT_SHA)" "${DOCKER_REGISTRY}:$$service-$(CI_COMMIT_SHORT_SHA)" && \
		docker tag "${DOCKER_REGISTRY}:django-latest" "${DOCKER_REGISTRY}:$$service-latest" ; \
	done

ci-push-images:
	@for service in $(RUN_ARGS) ; do \
		docker push "${DOCKER_REGISTRY}:$$service-$(CI_COMMIT_SHORT_SHA)" && \
		docker push "${DOCKER_REGISTRY}:$$service-latest" ; \
	done

# Formatting ##################################################################

format: clean ## Run code formatting and import sorting.
	black $(PACKAGE_NAME)
	isort $(PACKAGE_NAME)

# Development #################################################################

run: clean
	docker-compose up --remove-orphans

run.detach: clean
	docker-compose up --detach --remove-orphans

# Tests #######################################################################

test.quality: clean ## Run quality tests.
	mypy $(PACKAGE_NAME) && \
	flake8 $(PACKAGE_NAME) && \
	pylint $(PACKAGE_NAME)

test.pre-commit: ## Run pre-commit tests.
	pre-commit run --all

test.unit: clean ## Run all unit tests.
	$(PY_MANAGE_TEST_COVERAGE)

test.migrations: clean ## Check if migrations are missing.
	@$(PY_MANAGE) makemigrations --dry-run --check

test: clean test.quality test.unit test.migrations cov.html ## Run all tests.
	@echo "\nAll tests OK!\n"

test.one: clean ## Run one test or test suite.
	$(PY_MANAGE_TEST) $(RUN_ARGS)

# Files #######################################################################

cov.html: ## Generate html coverage
	coverage html
	@echo "\nCoverage HTML report at file://`pwd`/build/coverage/index.html\n"
	@coverage report

cov.clean:
	coverage erase
