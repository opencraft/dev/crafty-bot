import pytest

from crafty_bot.users.forms import UserCreationForm
from crafty_bot.users.tests.factories import UserFactory

pytestmark = pytest.mark.django_db


class TestUserCreationForm:  # pylint: disable=R0903
    def test_clean_username(self):  # pylint: disable=R0201
        # A user with proto_user params does not exist yet.
        proto_user = UserFactory.build()

        form = UserCreationForm(
            {
                "username": proto_user.username,
                "password1": proto_user._password,  # pylint: disable=W0212
                "password2": proto_user._password,  # pylint: disable=W0212
            }
        )

        assert form.is_valid()
        assert form.clean_username() == proto_user.username

        # Creating a user.
        form.save()

        # The user with proto_user params already exists,
        # hence cannot be created.
        form = UserCreationForm(
            {
                "username": proto_user.username,
                "password1": proto_user._password,  # pylint: disable=W0212
                "password2": proto_user._password,  # pylint: disable=W0212
            }
        )

        assert not form.is_valid()
        assert len(form.errors) == 1
        assert "username" in form.errors
