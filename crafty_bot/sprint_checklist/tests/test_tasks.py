"""
Test async task executions
"""

from unittest.mock import ANY, Mock, patch

import pytest
from django.conf import settings

from crafty_bot.sprint_checklist.models import Cell, Role
from crafty_bot.sprint_checklist.tasks import create_sprint_checklist_task


class TestAsyncTasks:  # pylint: disable=too-few-public-methods
    """
    Test celery task executions.
    """

    mock_board = Mock()
    mock_board.id = 1
    mock_board.name = "board"
    mock_board.state = "active"

    @pytest.mark.django_db
    @patch("crafty_bot.sprint_checklist.tasks.monday_client")
    @patch("crafty_bot.sprint_checklist.tasks.personalize_sprint_checklist_task")
    def test_sprint_checklist_creation_task(
        self, mock_personalize_sprint_checklist_task, mock_monday_client
    ):
        """
        Test sprint checklist creation.

        Test that based on the input data, we call the monday client with the
        expected arguments, also ensure that the sprint checklist
        personalization called with the necessary roles.
        """

        mock_monday_client.create_board_from_template.return_value = self.mock_board

        cell = Cell.objects.create(
            name="Test cell",
            abbreviation="CELL",
            workspace_id=1,
            evaluation_board_id=2,
            retrospective_board_id=3,
        )

        Role.objects.create(name="Sprint Firefighter", abbreviation="FF-1")
        Role.objects.create(name="Discovery Duty", abbreviation="DD-1")

        Role.objects.create(name="Client Owner", permanent=True)

        expected_roles = ["FF-1", "DD-1"]

        create_sprint_checklist_task(cell.name, 0, "member@example.com", expected_roles)

        mock_monday_client.create_board_from_template.assert_called_once_with(
            board_name="CELL.0 - Member",
            template_id=settings.MONDAY_SPRINT_CHECKLIST_TEMPLATE_ID,
            workspace_id=cell.workspace_id,
        )

        mock_personalize_sprint_checklist_task.apply_async.assert_called_once_with(
            (ANY, ["client owner", "discovery duty", "sprint firefighter"]),
            countdown=180,
            retry=True,
        )
