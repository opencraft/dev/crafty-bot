from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.views import TokenRefreshView

schema_view = get_schema_view(
    openapi.Info(
        title="Crafty Bot API",
        default_version="v1",
        description="OpenCraft automation bot API's documentation.",
        contact=openapi.Contact(email="help@opencraft.com"),
        license=openapi.License(name="GNU GPL 3 License"),
    ),
    public=True,
    permission_classes=[IsAuthenticated],
)

urlpatterns = [
    # Admin UI
    path(settings.ADMIN_URL, admin.site.urls),
    # Account and auth APIs
    path("accounts/", include("allauth.urls")),
    path("auth-token/", obtain_auth_token),
    path("rest-auth/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("rest-auth/", include("rest_auth.urls")),
    path("rest-auth/registration/", include("rest_auth.registration.urls")),
    # API base url
    path("api/", include("config.api_router")),
    # API Documentation
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # Static file serving when using Gunicorn + Uvicorn for local web socket development
    urlpatterns += staticfiles_urlpatterns()
