"""
API endpoints to interact with sprint checklists.
"""

from rest_framework.generics import GenericAPIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST

from crafty_bot.sprint_checklist.models import Cell, SprintChecklist
from crafty_bot.sprint_checklist.serializers import SprintChecklistSerializer
from crafty_bot.sprint_checklist.tasks import (
    archive_board_task,
    create_group_task,
    create_sprint_checklist_task,
)


class CreateSprintChecklistView(GenericAPIView):
    """
    Archive previous and create new sprint boards for every cell per member
    based on their role.
    """

    # queryset = SprintChecklist.objects.all()
    serializer_class = SprintChecklistSerializer

    # pylint: disable=unused-argument
    def post(self, request: Request, **kwargs) -> Response:
        """
        Create a new sprint checklist.

        If validation of input data passes, asynchronously archive the previous
        sprint's checklists and start creating new ones.
        """

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

        sprint_id: int = serializer.validated_data["sprint_number"]

        # Archive previous sprint checklists
        for checklist in SprintChecklist.objects.filter(sprint_id=sprint_id - 1):
            archive_board_task.apply_async((checklist.board_id,), retry=True)

        # Create sprint checklist for every member
        for email, roles in serializer.validated_data["participants"].items():
            create_sprint_checklist_task.delay(
                cell_name=serializer.validated_data["cell"],
                sprint_id=sprint_id,
                member=email,
                roles=roles,
            )

        # Create new groups in the Cell's evaluation and retrospective board for
        # the new sprint
        cell = Cell.objects.get(name=serializer.validated_data["cell"])
        group_name = f"{cell.abbreviation.upper()}.{sprint_id}"

        create_group_task.apply_async(
            (cell.evaluation_board_id, group_name), retry=True
        )

        create_group_task.apply_async(
            (cell.retrospective_board_id, group_name), retry=True
        )

        return Response(data=request.data, status=HTTP_201_CREATED)
