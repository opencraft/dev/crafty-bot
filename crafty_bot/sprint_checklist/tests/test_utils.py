"""
Test utility functions.
"""

# pylint: disable=no-self-use

from unittest.mock import call, patch

import pytest

from crafty_bot.sprint_checklist.utils import generate_abbreviation, get_name_from_email


class TestGenerateAbbreviation:
    """
    Test abbreviation generation for models.
    """

    @patch("crafty_bot.sprint_checklist.utils.Model")
    def test_generate_abbreviation(self, mock_sender):
        """
        Test generate abbreviation from a long name.

        Test that we don't need to generate a new name, since the model has no
        object with the same abbreviation.
        """

        expected_abbreviation = "tln"
        mock_sender.objects.filter.return_value.exists.return_value = False

        abbreviation = generate_abbreviation(
            sender=mock_sender,
            name="Test Long Name",
            max_length=0,  # Does not matter as the abbreviation does not exist
        )

        assert abbreviation == expected_abbreviation

        mock_sender.objects.filter.assert_called_once_with(
            abbreviation__iexact=expected_abbreviation.lower(),
        )

    @patch("crafty_bot.sprint_checklist.utils.random")
    @patch("crafty_bot.sprint_checklist.utils.Model")
    def test_generate_multiple_abbreviation(self, mock_sender, mock_random):
        """
        Test generate abbreviation from a long name.

        Test that we do need to generate a new name, since the model has some
        objects with the same abbreviation.
        """

        expected_abbreviation = "ghi"

        mock_random.choice.side_effect = ["A", "B", "C", "D", "E", "F", "G", "H", "I"]
        mock_sender.objects.filter.return_value.exists.side_effect = [
            True,
            True,
            True,
            False,
        ]

        abbreviation = generate_abbreviation(
            sender=mock_sender,
            name="Pretty Long Name",
            max_length=3,
        )

        assert abbreviation == expected_abbreviation

        mock_sender.objects.filter.assert_has_calls(
            [
                call(abbreviation__iexact="pln"),  # pretty long name
                call().exists(),
                call(abbreviation__iexact="abc"),
                call().exists(),
                call(abbreviation__iexact="def"),
                call().exists(),
                call(abbreviation__iexact=expected_abbreviation.lower()),
                call().exists(),
            ]
        )


class TestNameGenerationFromEmail:
    """
    Test cases for extracting name from email address.
    """

    def test_get_name_from_email(self):
        """
        Test extract first name from email address.
        """

        name = get_name_from_email("john@example.com")
        assert name == "John"

    def test_full_get_name_from_email(self):
        """
        Test extract full name from email address.
        """

        name = get_name_from_email("john.doe@example.com")
        assert name == "John Doe"

    def test_get_name_from_invalid_email(self):
        """
        Test first name cannot be extracted from invalid email address.
        """

        with pytest.raises(ValueError):
            get_name_from_email("john")

    def test_get_full_name_from_invalid_email(self):
        """
        Test full name cannot be extracted from invalid email address.
        """

        with pytest.raises(ValueError):
            get_name_from_email("john.doe")
