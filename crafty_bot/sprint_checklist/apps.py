"""
Django app registration of sprint checklist app.
"""

from django.apps import AppConfig


class SprintChecklistConfig(AppConfig):
    name = "crafty_bot.sprint_checklist"
    verbose_name = "Sprint Checklist"

    def ready(self) -> None:
        """
        Register signals for the app when it is ready.
        """

        # pylint: disable=import-outside-toplevel
        from crafty_bot.sprint_checklist import signals

        assert signals
