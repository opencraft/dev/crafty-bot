"""
Signal receivers registered for sprint checklists.
"""

from django.conf import settings
from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver

from crafty_bot.sprint_checklist.clients.monday import monday_client
from crafty_bot.sprint_checklist.models import Cell, Role
from crafty_bot.sprint_checklist.utils import generate_abbreviation


# pylint: disable=unused-argument
@receiver(pre_save, sender=Cell)
def generate_cell_abbreviation(sender, instance: Cell, **kwargs) -> None:
    """
    Generate an abbreviation for cell name if not set.
    """

    if instance.abbreviation:
        return

    instance.abbreviation = generate_abbreviation(
        sender=sender,
        name=instance.name,
        max_length=6,
    )


# pylint: disable=unused-argument
@receiver(pre_save, sender=Cell)
def generate_cell_workspace_id(sender, instance: Cell, **kwargs) -> None:
    """
    Create a workspace for the cell on monday.com if not exists.
    """

    if not instance.workspace_id:
        workspace = monday_client.create_workspace(name=instance.name)
        instance.workspace_id = workspace.id

    if not instance.evaluation_board_id:
        evaluation_board = monday_client.create_board_from_template(
            board_name="Sprint evaluation",
            template_id=settings.MONDAY_SPRINT_EVALUATION_TEMPLATE_ID,
            workspace_id=instance.workspace_id,
        )

        instance.evaluation_board_id = evaluation_board.id

    if not instance.retrospective_board_id:
        retrospective_board = monday_client.create_board_from_template(
            board_name="Sprint retrospective",
            template_id=settings.MONDAY_SPRINT_RETROSPECTIVE_TEMPLATE_ID,
            workspace_id=instance.workspace_id,
        )

        instance.retrospective_board_id = retrospective_board.id


# pylint: disable=unused-argument
@receiver(pre_save, sender=Role)
def generate_role_abbreviation(sender, instance: Role, **kwargs) -> None:
    """
    Generate an abbreviation for role name if not set.
    """

    if instance.abbreviation:
        return

    instance.abbreviation = generate_abbreviation(
        sender=sender,
        name=instance.name,
        max_length=6,
    )


# pylint: disable=unused-argument
@receiver(pre_save, sender=Role)
def ensure_lowercase_role_name_and_abbreviation(
    sender, instance: Role, **kwargs
) -> None:
    """
    Ensure that role names are in lowercase, even when created on the admin UI.
    """

    instance.name = instance.name.lower()

    if instance.abbreviation:
        instance.abbreviation = instance.abbreviation.lower()
