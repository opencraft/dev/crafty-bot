"""
Test crafty bot sprints checklists APIs.
"""

import json
from typing import Dict, List, Optional, Tuple
from unittest.mock import call, patch

import pytest
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.test import APIClient

from crafty_bot.sprint_checklist.models import Cell, SprintChecklist
from crafty_bot.sprint_checklist.serializers import EventName
from crafty_bot.users.models import User


@pytest.fixture()
def user_with_token() -> Tuple[User, Token]:
    """
    Create a user and a belonging token.

    This fixture ensures the test has a proper user and token set up to communicate
    with the APIs requiring authentication.
    """

    user = User.objects.create(username="testuser")
    token = Token.objects.create(user=user)
    return user, token


@pytest.fixture()
def previous_checklist() -> SprintChecklist:
    """
    Create a checklist for the previous sprint.

    This fixture ensures we can interact with a previous checklist which will be
    queried by the new sprint API.
    """

    cell = Cell.objects.create(
        name="cell",
        workspace_id=1,
        evaluation_board_id=2,
        retrospective_board_id=3,
    )

    return SprintChecklist.objects.create(
        board_id=0,
        board_name="Test board",
        state="active",
        cell=cell,
        sprint_id=0,
        member="user@example.com",
    )


# pylint: disable=redefined-outer-name,no-self-use
@pytest.mark.django_db
class TestCreateNewSprintAPI:
    """
    Test the API creates a new sprint.

    Test the API creates a new sprint when called with the right payload,
    otherwise, it should return HTTP BAD REQUEST to indicate the expected tasks
    won't start and sprint won't be created.
    """

    def __send_request(
        self,
        data: dict,
        token: Optional[Token] = None,
        client: Optional[APIClient] = None,
    ) -> Response:
        """
        Send a request to the desired API endpoint with the given data.
        """

        if not client:
            client = APIClient()
            client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        return client.post("/api/v1/checklist", data, format="json")

    @patch("crafty_bot.sprint_checklist.api.archive_board_task.apply_async")
    @patch("crafty_bot.sprint_checklist.api.create_sprint_checklist_task.apply_async")
    def test_create_new_sprint(  # pylint: disable=too-many-locals
        self,
        mock_create_sprint_checklist_task,
        mock_archive_board_task,
        user_with_token,
        previous_checklist,
    ):
        """
        Test create a new sprint.

        Create a new sprint with a valid and expected payload.
        """

        _, token = user_with_token

        expected_sprint_id = previous_checklist.sprint_id + 1
        expected_cell_name = "cell"
        expected_event_name = EventName.NEW_SPRINT.value

        normal_participant: Dict[str, List[str]] = {"normal.user@example.com": []}

        specific_participant_roles = ["Test Role"]
        specific_participant: Dict[str, List[str]] = {
            "specific.user@example.com": specific_participant_roles
        }

        expected_participants = dict()
        expected_participants.update(normal_participant)
        expected_participants.update(specific_participant)

        expected_payload = {
            "board_id": 1,  # Not used at the moment
            "cell": expected_cell_name,
            "sprint_name": "SE.239",  # Not used at the moment
            "sprint_number": expected_sprint_id,
            "participants": expected_participants,
            "event_name": expected_event_name,
        }

        response = self.__send_request(expected_payload, token=token)

        assert json.loads(response.content) == expected_payload
        assert response.status_code == status.HTTP_201_CREATED

        mock_archive_board_task.assert_called_once_with(
            (previous_checklist.board_id,), retry=True
        )

        mock_create_sprint_checklist_task.assert_has_calls(
            [
                call(
                    (),
                    {
                        "cell_name": expected_cell_name,
                        "sprint_id": expected_sprint_id,
                        "member": list(normal_participant.keys())[0],
                        "roles": [],
                    },
                ),
                call(
                    (),
                    {
                        "cell_name": expected_cell_name,
                        "sprint_id": expected_sprint_id,
                        "member": list(specific_participant.keys())[0],
                        "roles": specific_participant_roles,
                    },
                ),
            ]
        )

    def test_forbidden_request(self):
        """
        Test the user sending the request is not authenticated.

        In case of unauthenticated requests, we must return permission denied
        response since we don't want to allow anyone create a new sprint.
        """

        client = APIClient()
        response = self.__send_request({}, client=client)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_invalid_event_name(self, user_with_token):
        """
        Test invalid event name sent in payload.

        In case an invalid event name sent, indicate the reason of being invalid
        and return an HTTP BAD REQUEST letting the caller know that the request
        failed.
        """

        _, token = user_with_token
        response = self.__send_request({"event_name": "fake"}, token=token)
        response_content = json.loads(response.content)

        assert response_content["event_name"] == ['Event "fake" is not known.']
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_invalid_board_id(self, user_with_token):
        """
        Test invalid board ID sent in payload.

        In case an invalid board ID sent, indicate the reason of being invalid
        and return an HTTP BAD REQUEST letting the caller know that the request
        failed.
        """

        _, token = user_with_token
        response = self.__send_request({"board_id": 0}, token=token)
        response_content = json.loads(response.content)

        assert response_content["board_id"] == ["Board ID must be greater than zero."]
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_invalid_sprint_id(self, user_with_token):
        """
        Test invalid sprint ID sent in payload.

        In case an invalid sprint ID sent, indicate the reason of being invalid
        and return an HTTP BAD REQUEST letting the caller know that the request
        failed.
        """

        _, token = user_with_token
        response = self.__send_request({"sprint_number": 0}, token=token)
        response_content = json.loads(response.content)

        assert response_content["sprint_number"] == [
            "Sprint ID must be greater than zero."
        ]
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_invalid_cell(self, user_with_token):
        """
        Test invalid cell name sent in payload.

        In case an invalid cell name sent, indicate the reason of being invalid
        and return an HTTP BAD REQUEST letting the caller know that the request
        failed.
        """

        _, token = user_with_token
        response = self.__send_request({"cell": "fake"}, token=token)
        response_content = json.loads(response.content)

        assert response_content["cell"] == ['Cell "fake" is not known.']
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_invalid_participants_list(self, user_with_token):
        """
        Test invalid list of participants sent in payload.

        In case an invalid list of participants sent, indicate the reason of
        being invalid and return an HTTP BAD REQUEST letting the caller know
        that the request failed.
        """

        _, token = user_with_token
        response = self.__send_request(
            {"participants": {"user@example.com": [""]}}, token=token
        )
        response_content = json.loads(response.content)

        assert response_content["participants"] == [
            'Found empty roles for "user@example.com".'
        ]
        assert response.status_code == status.HTTP_400_BAD_REQUEST
