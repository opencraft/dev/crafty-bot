"""
Test Monday.com API client.
"""

import json
from unittest.mock import Mock, call, patch

import pytest
from django.conf import settings

from crafty_bot.sprint_checklist.clients.monday import (
    ColumnsAPIResponse,
    ColumnValuesAPIResponse,
    GenericAPIResponse,
    ItemsAPIResponse,
    MondayClient,
    UsersAPIResponse,
    WorkspacesAPIResponse,
    gql,
)


@pytest.fixture()
@patch("crafty_bot.sprint_checklist.clients.monday.Client")
def monday_client(_) -> MondayClient:
    """
    Fixture returning a patched MondayClient to ensure we don't call Monday.com.

    We cannot use the monday_client defined in clients.monday since that client
    is initialized with an unmocked GraphQL client.
    """

    client = MondayClient(
        url=settings.MONDAY_API_URL,
        token=settings.MONDAY_API_TOKEN,
    )

    client._execute = Mock()  # pylint: disable=protected-access

    return client


# pylint: disable=redefined-outer-name,protected-access,no-self-use
class TestMondayClient:
    """
    Test MondayClient functionalities.

    Test that we are able to call all the required operations with the expected
    queries, mutations and variables. The client is responsible for the communication
    with monday.com, hence it should be the single source of truth for retrieving data
    and the only component which can interact with monday.com.
    """

    expected_board_id = 123
    expected_board_name = "client test board"
    expected_column_id = "testcol"
    expected_column_text = "Test Column Value"
    expected_column_title = "Test Column"
    expected_item_id = 123321
    expected_item_name = "Item 1"
    expected_user_email = "crafty@opencraft.com"
    expected_user_id = 111
    expected_workspace_id = 321

    @patch("crafty_bot.sprint_checklist.clients.monday.Client")
    def test_client_query_execution(self, _):
        """
        Test query execution of GraphQL client called.

        Although the tested function is a private method, we need to ensure that
        the underlying GraphQL parameters are called with the right parameters.
        """

        expected_query = """
            query {
                boards {
                    id
                }
            }
        """

        expected_variables = {"var1": 1, "var2": 2}

        monday_client = MondayClient(
            url=settings.MONDAY_API_URL,
            token=settings.MONDAY_API_TOKEN,
        )

        monday_client._execute(
            query=expected_query,
            variables=expected_variables,
        )

        # The client is patched, hence pylint not recognizes its members
        # correctly
        # pylint: disable=no-member
        monday_client.client.execute.assert_called_once_with(
            gql(expected_query),
            variable_values=expected_variables,
        )

    def test_archive_board(self, monday_client: MondayClient):
        """
        Test archiving board.

        Test archiving a board called with the right query and variables and
        returns the expected dataclass.
        """

        expected_query = """
            mutation ($board_id: Int!) {
                archive_board(board_id: $board_id) {
                    id
                    name
                    state
                }
            }
        """

        expected_variables = {"board_id": self.expected_board_id}

        monday_client._execute.return_value = {
            "archive_board": {
                "id": self.expected_board_id,
                "name": self.expected_board_name,
                "state": "archived",
            },
        }

        result = monday_client.archive_board(self.expected_board_id)

        assert isinstance(result, GenericAPIResponse)
        assert result.id == self.expected_board_id
        assert result.name == self.expected_board_name
        assert result.state == "archived"

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_create_workspace(self, monday_client: MondayClient):
        """
        Test create a new workspace.

        Test creating a workspace called with the right query and variables and
        returns the expected dataclass.
        """

        expected_workspace_name = "Serenity"

        expected_query = """
            mutation ($name: String!) {
                create_workspace (name: $name, kind: open) {
                    id
                }
            }
        """

        expected_variables = {"name": expected_workspace_name}

        monday_client._execute.return_value = {
            "create_workspace": {
                "id": self.expected_workspace_id,
            },
        }

        result = monday_client.create_workspace(expected_workspace_name)

        assert isinstance(result, WorkspacesAPIResponse)
        assert result.id == self.expected_workspace_id

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_create_board_from_template(self, monday_client: MondayClient):
        """
        Test create a new board from a template.

        Test creating a board from a template called with the right query and
        variables and returns the expected dataclass.
        """

        template_id = 123

        expected_query = """
            mutation ($board_name: String!, $template_id: Int!, $workspace_id: Int!) {
                create_board(
                    board_name: $board_name,
                    board_kind: public,
                    template_id: $template_id,
                    workspace_id: $workspace_id
                ) {
                    id
                    name
                    state
                }
            }
        """

        expected_variables = {
            "board_name": self.expected_board_name,
            "template_id": template_id,
            "workspace_id": self.expected_workspace_id,
        }

        monday_client._execute.return_value = {
            "create_board": {
                "id": self.expected_board_id,
                # See the implementation for further notes on this return value
                "name": "Some duplicate temporary name",
                "state": "active",
            },
        }

        result = monday_client.create_board_from_template(
            self.expected_board_name,
            template_id,
            self.expected_workspace_id,
        )

        assert isinstance(result, GenericAPIResponse)
        assert result.id == self.expected_board_id
        assert result.name == self.expected_board_name
        assert result.state == "active"

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_delete_item(self, monday_client: MondayClient):
        """
        Test delete an item from a board.

        Test item deletion called with the right query and variables and returns
        the expected dataclass.
        """

        expected_item_id = 1234
        expected_item_name = "test item"

        expected_query = """
            mutation ($item_id: Int!) {
                delete_item (item_id: $item_id) {
                    id
                    name
                }
            }
        """

        expected_variables = {"item_id": expected_item_id}

        monday_client._execute.return_value = {
            "delete_item": {
                "id": expected_item_id,
                "name": expected_item_name,
            },
        }

        result = monday_client.delete_item(expected_item_id)

        assert isinstance(result, ItemsAPIResponse)
        assert result.id == expected_item_id
        assert result.name == expected_item_name

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_get_columns(self, monday_client: MondayClient):
        """
        Test get columns of a board.

        Test getting columns with the right query and variables and returns the
        expected dataclass.
        """

        expected_query = """
            query ($board_ids: [Int]) {
                boards(ids: $board_ids) {
                    columns {
                        id
                        title
                    }
                }
            }
        """

        expected_variables = {"board_ids": [self.expected_board_id]}

        monday_client._execute.return_value = {
            "boards": [
                {
                    "columns": [
                        {
                            "id": self.expected_column_id,
                            "title": self.expected_column_title,
                        }
                    ],
                }
            ],
        }

        result = monday_client.get_columns(self.expected_board_id)
        column = result[0]

        assert isinstance(result, list)
        assert isinstance(column, ColumnsAPIResponse)
        assert column.id == self.expected_column_id
        assert column.title == self.expected_column_title

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_get_board_items(self, monday_client: MondayClient):
        """
        Test get items for a board.

        Test getting items with the right query and variables and returns the
        expected dataclass.
        """

        expected_query = """
            query ($board_ids: [Int]) {
                boards(ids: $board_ids) {
                    items {
                        id
                        name
                        column_values {
                            id
                            text
                        }
                    }
                }
            }
        """

        expected_variables = {"board_ids": [self.expected_board_id]}

        monday_client._execute.return_value = {
            "boards": [
                {
                    "items": [
                        {
                            "id": self.expected_item_id,
                            "name": self.expected_item_name,
                            "column_values": [
                                {
                                    "id": self.expected_column_id,
                                    "text": self.expected_column_text,
                                },
                            ],
                        }
                    ],
                }
            ]
        }

        result = monday_client.get_board_items(self.expected_board_id)
        item = result[0]

        assert isinstance(result, list)
        assert isinstance(item, ItemsAPIResponse)
        assert item.id == self.expected_item_id
        assert item.name == self.expected_item_name
        assert item.column_values == [
            ColumnValuesAPIResponse(
                id=self.expected_column_id, text=self.expected_column_text
            )
        ]

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_get_items_by_column_value(self, monday_client: MondayClient):
        """
        Test get items for a board by their column values.

        Test getting items by their column values with the right query and
        variables and returns the expected dataclass.
        """

        expected_query = """
            query ($board_id: Int!, $column_id: String!, $column_value: String!) {
                items_by_column_values (
                    board_id: $board_id,
                    column_id: $column_id,
                    column_value: $column_value
                ) {
                    id
                    name
                    state
                }
            }
        """

        expected_variables = {
            "board_id": self.expected_board_id,
            "column_id": self.expected_column_id,
            "column_value": self.expected_column_text,
        }

        monday_client._execute.return_value = {
            "items_by_column_values": [
                {
                    "id": self.expected_item_id,
                    "name": self.expected_item_name,
                    "state": "active",
                }
            ],
        }

        result = monday_client.get_items_by_column_values(
            self.expected_board_id,
            self.expected_column_id,
            self.expected_column_text,
        )

        item = result[0]

        assert isinstance(result, list)
        assert isinstance(item, GenericAPIResponse)
        assert item.id == self.expected_item_id
        assert item.name == self.expected_item_name
        assert item.state == "active"

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_find_column_by_title(self, monday_client: MondayClient):
        """
        Test find a column ID by the column's title.

        Test getting column ID by its column title with the right query and
        variables and returns the expected dataclass.
        """

        monday_client.get_columns = Mock()
        monday_client.get_columns.return_value = [
            ColumnsAPIResponse(
                id="other_column",
                title="other title",
            ),
            ColumnsAPIResponse(
                id=self.expected_column_id,
                title=self.expected_column_title,
            ),
            ColumnsAPIResponse(
                id="another_column",
                title="another title",
            ),
        ]

        result = monday_client.find_column_by_title(
            self.expected_board_id, self.expected_column_title
        )

        assert isinstance(result, ColumnsAPIResponse)
        assert result.id == self.expected_column_id
        assert result.title == self.expected_column_title

        monday_client.get_columns.assert_called_once_with(self.expected_board_id)

    def test_find_user(self, monday_client: MondayClient):
        """
        Test finding a user by its email address.

        Test finding a user by its email address called with the right query and
        variables and returns the expected dataclass.
        """

        expected_user = {
            "id": 2,
            "name": "John Doe",
            "email": "john.doe@example.com",
        }

        expected_users = [
            {
                "id": 1,
                "name": "Jane Doe",
                "email": "jane.doe@example.com",
            },
            expected_user,
            {
                "id": 3,
                "name": "Johanna Doe",
                "email": "johanna.doe@example.com",
            },
        ]

        expected_query = """
            query {
                users {
                    id
                    name
                    email
                }
            }
        """

        monday_client._execute.return_value = {"users": expected_users}

        result = monday_client.find_user(expected_user["email"])

        assert isinstance(result, UsersAPIResponse)
        assert result.id == expected_user["id"]
        assert result.name == expected_user["name"]
        assert result.email == expected_user["email"]

        monday_client._execute.assert_called_once_with(expected_query)

    def test_change_column(self, monday_client: MondayClient):
        """
        Test change the value of a column.

        Test changing column values called with the right query and variables
        and returns the expected dataclass.
        """

        expected_value = {"key": "value"}

        expected_query = """
            mutation (
                    $board_id: Int!,
                    $item_id: Int,
                    $column_id: String!,
                    $value: JSON!
                ) {
                change_column_value(
                    board_id: $board_id,
                    item_id: $item_id,
                    column_id: $column_id,
                    value: $value
                ) {
                    id
                    name
                }
            }
        """

        expected_variables = {
            "board_id": self.expected_board_id,
            "item_id": self.expected_item_id,
            "column_id": self.expected_column_id,
            "value": json.dumps(expected_value),
        }

        monday_client._execute.return_value = {
            "change_column_value": {
                "id": self.expected_item_id,
                "name": self.expected_item_name,
            }
        }

        result = monday_client.change_column_value(
            self.expected_board_id,
            self.expected_item_id,
            self.expected_column_id,
            expected_value,
        )

        assert isinstance(result, ItemsAPIResponse)
        assert result.id == self.expected_item_id
        assert result.name == self.expected_item_name

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_remove_items_by_column_value(self, monday_client: MondayClient):
        """
        Test removing items by their column value.

        Test removing items by their column value called with the right query
        and variables and returns the expected dataclass.
        """

        expected_items = [
            ItemsAPIResponse(123, self.expected_item_name),
            ItemsAPIResponse(456, self.expected_item_name),
            ItemsAPIResponse(789, self.expected_item_name),
        ]

        expected_value = "some value"

        monday_client.get_items_by_column_values = Mock()
        monday_client.get_items_by_column_values.return_value = expected_items

        monday_client.delete_item = Mock()

        monday_client.remove_items_by_column_value(
            self.expected_board_id,
            self.expected_column_id,
            expected_value,
        )

        monday_client.get_items_by_column_values.assert_called_once_with(
            board_id=self.expected_board_id,
            column_id=self.expected_column_id,
            value=expected_value,
        )

        monday_client.delete_item.assert_has_calls(
            [
                call(item_id=expected_items[0].id),
                call(item_id=expected_items[1].id),
                call(item_id=expected_items[2].id),
            ]
        )

    def test_get_column_values(self, monday_client: MondayClient):
        """
        Test getting all unique values of a column.

        Test getting unique column values called with the right query and
        variables and returns the expected dataclass.
        """

        expected_query = """
            query ($board_id: Int!) {
                boards (ids: [$board_id]) {
                    items {
                        column_values {
                            id
                            text
                        }
                    }
                }
            }
        """

        expected_variables = {
            "board_id": self.expected_board_id,
        }

        monday_client._execute.return_value = {
            "boards": [
                {
                    "items": [
                        {
                            "column_values": [
                                {
                                    "id": self.expected_column_id,
                                    "text": "any text",
                                },
                                {
                                    "id": self.expected_column_id,
                                    "text": "other text",
                                },
                            ]
                        },
                        {
                            "column_values": [
                                {
                                    "id": self.expected_column_id,
                                    "text": "any text",
                                },
                                {
                                    "id": self.expected_column_id,
                                    "text": "another text",
                                },
                            ]
                        },
                    ]
                }
            ]
        }

        result = monday_client.get_column_values(
            self.expected_board_id,
            self.expected_column_id,
        )

        assert len(result) == 3
        assert isinstance(result, set)
        assert all([isinstance(value, str) for value in result])

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_add_owner_to_board(self, monday_client: MondayClient):
        """
        Test adding an additional owner to a board.

        Test adding an additional owner to a board called with the right query
        and variables and returns the expected dataclass.
        """

        monday_client.find_user = Mock()
        monday_client.find_user.return_value = Mock(id=self.expected_user_id)

        expected_query = """
            mutation ($board_id: Int!, $user_ids: [Int]!) {
                add_subscribers_to_board (
                    board_id: $board_id,
                    user_ids: $user_ids,
                    kind: owner
                ) {
                    id
                }
            }
        """

        expected_variables = {
            "board_id": self.expected_board_id,
            "user_ids": [self.expected_user_id],
        }

        monday_client.add_owner_to_board(
            self.expected_board_id, self.expected_user_email
        )

        monday_client.find_user.assert_called_once_with(
            email=self.expected_user_email,
        )

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_add_no_owner_to_board(self, monday_client: MondayClient):
        """
        Test adding no additional owners to a board.

        Test adding no additional owners to a board called with the right query
        and variables and returns the expected dataclass.
        """

        monday_client.find_user = Mock()
        monday_client.find_user.return_value = None

        monday_client.add_owner_to_board(
            self.expected_board_id, self.expected_user_email
        )

        monday_client.find_user.assert_called_once_with(
            email=self.expected_user_email,
        )

        assert monday_client._execute.called is False

    def test_add_subscriber_to_board(self, monday_client: MondayClient):
        """
        Test adding an additional subscriber to a board.

        Test adding an additional subscriber to a board called with the right
        query and variables and returns the expected dataclass.
        """

        monday_client.find_user = Mock()
        monday_client.find_user.return_value = Mock(id=self.expected_user_id)

        expected_query = """
            mutation ($board_id: Int!, $user_ids: [Int]!) {
                add_subscribers_to_board (
                    board_id: $board_id,
                    user_ids: $user_ids,
                    kind: subscriber
                ) {
                    id
                }
            }
        """

        expected_variables = {
            "board_id": self.expected_board_id,
            "user_ids": [self.expected_user_id],
        }

        monday_client.add_subscriber_to_board(
            self.expected_board_id, [self.expected_user_email]
        )

        monday_client.find_user.assert_called_once_with(
            email=self.expected_user_email,
        )

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_add_no_subscriber_to_board(self, monday_client: MondayClient):
        """
        Test adding no additional subscribers to a board.

        Test adding an additional subscriber to a board called with the right
        query and variables and returns the expected dataclass.
        """

        monday_client.find_user = Mock()
        monday_client.find_user.return_value = None

        monday_client.add_subscriber_to_board(
            self.expected_board_id, [self.expected_user_email]
        )

        monday_client.find_user.assert_called_once_with(
            email=self.expected_user_email,
        )

        assert monday_client._execute.called is False

    def test_create_group(self, monday_client: MondayClient):
        """
        Test creating a new group in a board.

        Test creating a group in a board called with the right query and
        variables and returns the expected dataclass.
        """

        expected_group_id = 1
        expected_group_name = "test group"

        expected_query = """
            mutation ($board_id: Int!, $group_name: String!) {
                create_group (board_id: $board_id, group_name: $group_name) {
                    id
                }
            }
        """

        expected_variables = {
            "board_id": self.expected_board_id,
            "group_name": expected_group_name,
        }

        monday_client._execute.return_value = {
            "create_group": {
                "id": expected_group_id,
            },
        }

        monday_client.create_group(self.expected_board_id, expected_group_name)

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )
