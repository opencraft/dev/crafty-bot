# Generated by Django 3.0.11 on 2021-10-19 07:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sprint_checklist', '0004_cell_checklist_template_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cell',
            name='name',
            field=models.CharField(help_text='Full name of the cell.', max_length=120, unique=True),
        ),
        migrations.AlterField(
            model_name='role',
            name='name',
            field=models.CharField(help_text='Full name of the role in lowercase.', max_length=120, unique=True),
        ),
    ]
