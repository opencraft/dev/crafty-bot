"""
Asynchronous, tasks to create and manipulate sprint checklists.
"""

from datetime import date
from typing import List

from django.conf import settings
from django.db.models import Q

from config import celery_app
from config.logger import logger
from crafty_bot.sprint_checklist.clients.monday import monday_client
from crafty_bot.sprint_checklist.models import Cell, Role, SprintChecklist
from crafty_bot.sprint_checklist.utils import get_name_from_email


@celery_app.task()
def create_group_task(board_id: int, group_name: str):
    """
    Create a new group with the given name.
    """

    monday_client.create_group(
        board_id=board_id,
        group_name=group_name,
    )


@celery_app.task()
def archive_board_task(board_id: int):
    """
    Archive a board based on its ID.
    """

    response = monday_client.archive_board(board_id=board_id)

    # Using filter + update to reduce the SQL statements executed
    SprintChecklist.objects.filter(board_id=board_id).update(state=response.state)


@celery_app.task(time_limit=310, soft_time_limit=300)
def personalize_sprint_checklist_task(checklist_id: int, roles_to_keep: List[str]):
    """
    Remove checklist items which are not relevant for the givent role.

    Ensure that the sprint checklist has only those fileds which are relevant
    for the given role. Remove those items from the table which belongs to other
    roles.
    """

    checklist = SprintChecklist.objects.get(id=checklist_id)

    role_column = monday_client.find_column_by_title(
        board_id=checklist.board_id, title="Role"
    )

    if role_column is None:
        raise ValueError(f'No columns found for Role for board "{checklist.board_id}".')

    roles = monday_client.get_column_values(
        board_id=checklist.board_id, column_id=role_column.id
    )

    roles_to_remove = set(roles).difference(set(roles_to_keep))

    for role in roles_to_remove:
        monday_client.remove_items_by_column_value(
            board_id=checklist.board_id,
            column_id=role_column.id,
            value=role,
        )

    monday_client.set_timeline(board_id=checklist.board_id, start_date=date.today())
    monday_client.assign_item(board_id=checklist.board_id, email=checklist.member)

    additional_subscribers = SprintChecklist.objects.filter(
        sprint_id=checklist.sprint_id,
        cell=checklist.cell,
        roles__subscribe_to_boards=True,
    ).values_list("member", flat=True)

    monday_client.add_subscriber_to_board(
        board_id=checklist.board_id,
        subscribers=list(additional_subscribers),
    )

    monday_client.add_owner_to_board(
        board_id=checklist.board_id,
        email=checklist.member,
    )


@celery_app.task()
def create_sprint_checklist_task(
    cell_name: str, sprint_id: int, member: str, roles: List[str]
):
    """
    Start a new sprint as it is started in Sprints app.

    Create a new Sprint Checklist from template indicating that a new sprint
    started.
    """

    cell = Cell.objects.get(name=cell_name)
    roles = [role.lower() for role in roles]

    board_name = f"{cell.abbreviation}.{sprint_id} - {get_name_from_email(member)}"

    # If a board exists with the same name, do not create a new board
    if SprintChecklist.objects.filter(board_name=board_name).exists():
        logger.info('Checklist "%s" already exists.', board_name)
        return

    # Create roles if not exists at the beginning of the process to not
    # schedule further tasks if role creation fails
    for role in roles:
        role_exists = Role.objects.filter(
            Q(name=role) | Q(abbreviation__iexact=role)
        ).exists()

        if not role_exists:
            Role.objects.create(name=role)

    # Template configured in the cell
    template_id = cell.checklist_template_id

    # If there is no template, then set the default template
    if template_id is None:
        template_id = settings.MONDAY_SPRINT_CHECKLIST_TEMPLATE_ID

    response = monday_client.create_board_from_template(
        board_name=board_name,
        template_id=template_id,
        workspace_id=cell.workspace_id,
    )

    checklist = SprintChecklist.objects.create(
        board_id=response.id,
        board_name=response.name,
        state=response.state,
        cell=cell,
        sprint_id=sprint_id,
        member=member,
    )

    assigned_roles = Role.objects.filter(
        Q(permanent=True) | Q(name__in=roles) | Q(abbreviation__in=roles)
    )

    checklist.roles.add(*assigned_roles)

    # Start the task in 3 minutes as Monday.com need some time to propagate
    # board creation
    personalize_sprint_checklist_task.apply_async(
        (checklist.id, [role.name for role in checklist.roles.all()]),
        countdown=180,
        retry=True,
    )
