"""
Client and related helper classes to interact with Monday.com.
"""

import json
from dataclasses import dataclass
from datetime import date, timedelta
from typing import List, Optional, Set

from django.conf import settings
from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport


@dataclass
class ColumnsAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating a board's columns.
    """

    id: str
    title: str


@dataclass
class ColumnValuesAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating a board's columns.
    """

    id: int
    text: str


@dataclass
class WorkspacesAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating a workspace.
    """

    id: int


@dataclass
class ItemsAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating a board's items.
    """

    id: int
    name: str
    column_values: Optional[List[ColumnValuesAPIResponse]] = None


@dataclass
class UsersAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating a users related to Monday.com account.
    """

    id: int
    name: str
    email: str


@dataclass
class GenericAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating a board.
    """

    id: int
    name: str
    state: str


class MondayClient:
    """
    Client implementation to communicate with Monday.com GraphQL API.
    """

    def __init__(self, url: str, token: str) -> None:
        timeout = 60

        self.client = Client(
            transport=RequestsHTTPTransport(
                url=url,
                headers={"Authorization": token},
                timeout=timeout,  # Monday.com responds slowly for some requests
            ),
            execute_timeout=timeout,  # Use the same timeout as for HTTP requests
        )

    def _execute(
        self,
        query: str,
        variables: Optional[dict] = None,
    ) -> dict:
        """
        Execute a query against monday.com API.
        """

        # NOTE: Possible improvement to use a cache here.
        return self.client.execute(gql(query), variable_values=variables or dict())

    def archive_board(self, board_id: int) -> GenericAPIResponse:
        """
        Archive an existing board based on its id on Monday.com.
        """

        query = """
            mutation ($board_id: Int!) {
                archive_board(board_id: $board_id) {
                    id
                    name
                    state
                }
            }
        """

        response = self._execute(query, variables={"board_id": board_id})
        return GenericAPIResponse(**response["archive_board"])

    def create_workspace(self, name: str) -> WorkspacesAPIResponse:
        """
        Create a workspace on monday.com.
        """

        query = """
            mutation ($name: String!) {
                create_workspace (name: $name, kind: open) {
                    id
                }
            }
        """

        response = self._execute(query, variables={"name": name})
        return WorkspacesAPIResponse(**response["create_workspace"])

    def create_board_from_template(
        self, board_name: str, template_id: int, workspace_id: int
    ) -> GenericAPIResponse:
        """
        Create a new board on Monday.com based on the given template id.
        """

        query = """
            mutation ($board_name: String!, $template_id: Int!, $workspace_id: Int!) {
                create_board(
                    board_name: $board_name,
                    board_kind: public,
                    template_id: $template_id,
                    workspace_id: $workspace_id
                ) {
                    id
                    name
                    state
                }
            }
        """

        response = self._execute(
            query,
            variables={
                "board_name": board_name,
                "template_id": template_id,
                "workspace_id": workspace_id,
            },
        )

        # Get the response and rename board as the original board name returned
        # by the API which is the "Duplicate of ..." though the board is created
        # with the expected name.
        response["create_board"]["name"] = board_name

        return GenericAPIResponse(**response["create_board"])

    def delete_item(self, item_id: int) -> ItemsAPIResponse:
        """
        Delete an item on a board by item ID.
        """

        query = """
            mutation ($item_id: Int!) {
                delete_item (item_id: $item_id) {
                    id
                    name
                }
            }
        """

        response = self._execute(query, variables={"item_id": int(item_id)})
        return ItemsAPIResponse(**response["delete_item"])

    def get_columns(self, board_id: int) -> List[ColumnsAPIResponse]:
        """
        Return the list of columns and its ids related to a board.
        """

        query = """
            query ($board_ids: [Int]) {
                boards(ids: $board_ids) {
                    columns {
                        id
                        title
                    }
                }
            }
        """

        response = self._execute(query, variables={"board_ids": [int(board_id)]})
        columns = response["boards"][0]["columns"]
        return [ColumnsAPIResponse(**column) for column in columns]

    def get_board_items(self, board_id: int) -> List[ItemsAPIResponse]:
        """
        Return the list of items created on a given board.
        """

        query = """
            query ($board_ids: [Int]) {
                boards(ids: $board_ids) {
                    items {
                        id
                        name
                        column_values {
                            id
                            text
                        }
                    }
                }
            }
        """

        response = self._execute(query, variables={"board_ids": [int(board_id)]})
        items = response["boards"][0]["items"]

        return [
            ItemsAPIResponse(
                id=item["id"],
                name=item["name"],
                column_values=[
                    ColumnValuesAPIResponse(**column)
                    for column in item["column_values"]
                ],
            )
            for item in items
        ]

    def get_items_by_column_values(
        self, board_id: int, column_id: str, value: str
    ) -> List[GenericAPIResponse]:
        """
        Query items by column values and return them as an APIResponse.
        """

        query = """
            query ($board_id: Int!, $column_id: String!, $column_value: String!) {
                items_by_column_values (
                    board_id: $board_id,
                    column_id: $column_id,
                    column_value: $column_value
                ) {
                    id
                    name
                    state
                }
            }
        """

        response = self._execute(
            query,
            variables={
                "board_id": int(board_id),
                "column_id": column_id,
                "column_value": value,
            },
        )

        return [
            GenericAPIResponse(**item) for item in response["items_by_column_values"]
        ]

    def find_column_by_title(
        self, board_id: int, title: str
    ) -> Optional[ColumnsAPIResponse]:
        """
        Return the column ID for board by title.

        Since the search is case-sensitive, in case the board has no column with
        the exact title, None will be returned.

        If multiple columns has the same title, the first found column_id will
        return.
        """

        matching_columns = filter(
            lambda column: column.title.lower() == title.lower(),
            self.get_columns(board_id),
        )

        return next(matching_columns, None)

    def find_user(self, email: str) -> Optional[UsersAPIResponse]:
        """
        Find user by its email address.

        List all users registered to monday.com account and find the requested
        user by its email address. In case no user found, return None.
        """

        query = """
            query {
                users {
                    id
                    name
                    email
                }
            }
        """

        response = self._execute(query)
        matching_emails = filter(
            lambda u: u.email == email,
            [UsersAPIResponse(**user) for user in response["users"]],
        )

        return next(matching_emails, None)

    def change_column_value(
        self, board_id: int, item_id: int, column_id: str, value: dict
    ) -> ItemsAPIResponse:
        """
        Change a column's value on a board, identified by the item's and column's
        ID.
        """

        query = """
            mutation (
                    $board_id: Int!,
                    $item_id: Int,
                    $column_id: String!,
                    $value: JSON!
                ) {
                change_column_value(
                    board_id: $board_id,
                    item_id: $item_id,
                    column_id: $column_id,
                    value: $value
                ) {
                    id
                    name
                }
            }
        """

        response = self._execute(
            query,
            variables={
                "board_id": int(board_id),
                "item_id": int(item_id),
                "column_id": str(column_id),
                "value": json.dumps(value),
            },
        )

        return ItemsAPIResponse(**response["change_column_value"])

    def remove_items_by_column_value(
        self, board_id: int, column_id: str, value: str
    ) -> None:
        """
        Remove items from the given board based on a column's value.

        In case the items cannot be found by the column's value, no changes will
        be made.
        """

        items = self.get_items_by_column_values(
            board_id=board_id,
            column_id=column_id,
            value=value,
        )

        for item in items:
            self.delete_item(item_id=item.id)

    def get_column_values(self, board_id: int, column_id: str) -> Set[str]:
        """
        Return the values set for a given board's column.

        The returned values are unique. Duplications will be removed.
        """

        query = """
            query ($board_id: Int!) {
                boards (ids: [$board_id]) {
                    items {
                        column_values {
                            id
                            text
                        }
                    }
                }
            }
        """

        response = self._execute(query, variables={"board_id": int(board_id)})
        column_values: List[ColumnValuesAPIResponse] = list()

        for item in response["boards"][0]["items"]:
            for column in item["column_values"]:
                column_values.append(ColumnValuesAPIResponse(**column))

        values = filter(lambda v: v.id == column_id, column_values)
        return {value.text for value in values}

    def add_owner_to_board(self, board_id: int, email: str) -> None:
        """
        Add subscriber to a board as an owner.

        The already assigned owners will remain as they are, the API will extend
        the existing list of owners.
        """

        query = """
            mutation ($board_id: Int!, $user_ids: [Int]!) {
                add_subscribers_to_board (
                    board_id: $board_id,
                    user_ids: $user_ids,
                    kind: owner
                ) {
                    id
                }
            }
        """

        user = self.find_user(email=email)
        if not user:
            return

        self._execute(
            query,
            variables={
                "board_id": int(board_id),
                "user_ids": [user.id],
            },
        )

    def add_subscriber_to_board(self, board_id: int, subscribers: List[str]) -> None:
        """
        Add a subscriber to a board.

        Similarly to adding an owner, the existing subscribers won't be affected
        by this change.
        """

        query = """
            mutation ($board_id: Int!, $user_ids: [Int]!) {
                add_subscribers_to_board (
                    board_id: $board_id,
                    user_ids: $user_ids,
                    kind: subscriber
                ) {
                    id
                }
            }
        """

        users = [self.find_user(email=email) for email in subscribers]
        user_ids = [user.id for user in users if user]

        if not user_ids:
            return

        self._execute(
            query,
            variables={
                "board_id": int(board_id),
                "user_ids": user_ids,
            },
        )

    def assign_item(self, board_id: int, email: str) -> None:
        """
        Assign all items on a board to a user.
        """

        user = self.find_user(email=email)

        if not user:
            raise ValueError(f'No user found with email "{email}".')

        column = self.find_column_by_title(board_id=board_id, title="Assignee")

        if not column:
            raise ValueError('No column ID found for title "Assignee".')

        for item in self.get_board_items(board_id=board_id):
            self.change_column_value(
                board_id=board_id,
                item_id=item.id,
                column_id=column.id,
                value={"personsAndTeams": [{"id": user.id, "kind": "person"}]},
            )

    def set_timeline(self, board_id: int, start_date: date) -> None:
        """
        Set timeline for items on the user's sprint checklist.

        We assume that the sprint started on Tuesday as the sprint start will be
        at the end of day Monday UTC. This assumption won't be true if the server
        date changes from UTC+0. The `start_date` defined accordingly.
        """

        def get_column_text(
            column_values: List[ColumnValuesAPIResponse], column_id: str
        ):
            column = next(
                filter(lambda column: column.id == column_id, column_values), None
            )

            if not column:
                raise ValueError(f'No column ID found for "{column_id}".')

            return column.text

        timeline_column = self.find_column_by_title(board_id=board_id, title="Timeline")
        start_day_column = self.find_column_by_title(
            board_id=board_id, title="Start day"
        )
        end_day_column = self.find_column_by_title(board_id=board_id, title="End day")

        if not timeline_column or not start_day_column or not end_day_column:
            raise ValueError("No start or end day column found.")

        items = self.get_board_items(board_id=board_id)

        for item in items:
            start_day = int(
                get_column_text(item.column_values or list(), start_day_column.id)
            )
            end_day = int(
                get_column_text(item.column_values or list(), end_day_column.id)
            )

            self.change_column_value(
                board_id=board_id,
                item_id=item.id,
                column_id=timeline_column.id,
                value={
                    "from": (start_date + timedelta(days=start_day)).isoformat(),
                    "to": (start_date + timedelta(days=end_day)).isoformat(),
                },
            )

    def create_group(self, board_id: int, group_name: str) -> None:
        """
        Create new group within a board with the given name.

        Note: in case the group already exists, a new group will be created with
        the given name.
        """

        query = """
            mutation ($board_id: Int!, $group_name: String!) {
                create_group (board_id: $board_id, group_name: $group_name) {
                    id
                }
            }
        """

        self._execute(
            query,
            variables={
                "board_id": int(board_id),
                "group_name": group_name,
            },
        )


# Initialize the client with default settings
monday_client = MondayClient(
    url=settings.MONDAY_API_URL,
    token=settings.MONDAY_API_TOKEN,
)
