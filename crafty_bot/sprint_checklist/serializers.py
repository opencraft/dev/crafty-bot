"""
API request/response serializers of sprint checklist endpoints.
"""

from enum import Enum
from typing import Dict, List

from django.db.models import Q
from rest_framework import serializers

from crafty_bot.sprint_checklist.models import Cell


# pylint: disable=abstract-method
class EventName(Enum):
    """
    Possible event names known by sprint checklists.
    """

    NEW_SPRINT: str = "new sprint"

    @classmethod
    def values(cls) -> List[str]:
        """
        Return the list representation of possible values.
        """

        return [i.value for i in cls]


# pylint: disable=abstract-method
class SprintChecklistSerializer(serializers.Serializer):
    """
    Serialize the incoming data sent by Sprints app.
    """

    board_id = serializers.IntegerField(help_text="")
    cell = serializers.CharField()
    sprint_number = serializers.IntegerField()
    sprint_name = serializers.CharField()
    participants = serializers.DictField()
    event_name = serializers.CharField()

    # pylint: disable=no-self-use
    def validate_board_id(self, value: int) -> int:
        """
        Validate the board ID must be greater that 0.
        """

        if value < 1:
            raise serializers.ValidationError("Board ID must be greater than zero.")

        return value

    # pylint: disable=no-self-use
    def validate_cell(self, value: str) -> str:
        """
        Validate that cell exists.

        If cell not exists, return a validation error.
        """

        try:
            Cell.objects.get(Q(name__iexact=value) | Q(abbreviation__iexact=value))
        except Cell.DoesNotExist:
            # pylint: disable=raise-missing-from
            raise serializers.ValidationError(f'Cell "{value}" is not known.')

        return value

    # pylint: disable=no-self-use
    def validate_sprint_number(self, value: int) -> int:
        """
        Ensure sprint number is greater than zero.
        """

        if value < 1:
            raise serializers.ValidationError("Sprint ID must be greater than zero.")

        return value

    # pylint: disable=no-self-use
    def validate_participants(
        self, value: Dict[str, List[str]]
    ) -> Dict[str, List[str]]:
        """
        Ensure participant roles does not contain empty strings.
        """

        if not value:
            raise serializers.ValidationError("No participants provided.")

        for email, roles in value.items():
            has_empty_role = any([role == "" for role in roles])

            if not isinstance(roles, list) or has_empty_role:
                raise serializers.ValidationError(f'Found empty roles for "{email}".')

        return value

    # pylint: disable=no-self-use
    def validate_event_name(self, value: str) -> str:
        """
        Check if the event name is known by us.

        In case the event is not known, raise a validation error, as we cannot
        handle the request due to invalid payload.
        """

        if value.lower() not in EventName.values():
            raise serializers.ValidationError(f'Event "{value}" is not known.')

        return value
