Troubleshooting
===============

This guide helps you troubleshoot and fix the most common issues about the communication between the Sprints and Crafty bot apps.

Debugging crafty bot
--------------------

Crafty bot is integrated with `Sentry <https://sentry.io/organizations/opencraft/projects/crafty-bot/?project=5572527&statsPeriod=14d>`_\ , which is an excellent source of information about failures. To get started, log in to Sentry, using the `secrets <https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft%20Tools%20:%20Resources%20:%20Sentry>`_ in Vault.

In the case you see issues marked as new ones, probably that's the issue and it should be obvious how to fix that. Othervise, you will need to check the docker processes on the instance (\ ``crafty.opencraft.com``\ ). The app is running in docker containers, started by docker-compose, located at ``/crafty``.

To log in and check stdout and stderr of the containers, run the following:

.. code-block:: shell

   $ crafty.opencraft.com
   $ cd /crafty/
   $ docker-compose logs django

XY's board is not created on Monday.com
---------------------------------------

This is the most common issue since the Sprints app does hiccup from time to time. To fix the issue, first, you need to get more insights. When you made sure that the root cause is fixed, you need to do the following:


#. Login to `Monday.com <https://opencraft.monday.com>`_
#. Select the workspace where the board is missing
#. Double-check that the board is not there
#. Create a superuser for yourself if you don't have already (on the instance, in the ``/crafty`` directory, run ``docker-compose run --rm django python manage.py createsuperuser``\ )
#. Login to https://crafty.opencraft.com/\ :raw-html-m2r:`<ADMIN PATH>`\ , where the :raw-html-m2r:`<ADMIN PATH>` is `stored in Vault <https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft%20Tools%20:%20Crafty%20Bot>`_
#. Within the ``/crafty`` directory, run ``docker-compose run --rm django python manage.py shell`` to get a shell
#. Run the script below with the correct parameters. We do not want to override a board for someone else

**Script to run for creating a board properly**

.. code-block:: python

   from crafty_bot.sprint_checklist.tasks import create_sprint_checklist_task

   CELL_NAME: str = "Falcon"
   SPRINT_ID: int = 258  # Replace with the current sprint ID
   MEMBER = "jill@opencraft.com"  # The missing board's owner
   # List of roles the member has
   # Hint: the list of roles usually there in the "Additional Data" section on the Sentry issue page
   ROLES = [
       "Epic planning and sustainability manager",
       "FF-2"
   ]

   create_sprint_checklist_task(CELL_NAME, SPRINT_ID, MEMBER, ROLES)
