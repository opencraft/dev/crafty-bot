"""
Test models registered for sprint checklists
"""

from unittest.mock import Mock, call, patch

import pytest
from django.conf import settings

from crafty_bot.sprint_checklist.models import Cell, Role, SprintChecklist


@pytest.mark.django_db
class TestCellModel:
    """
    Test cell model functionalities.
    """

    expected_name = "test cell"
    expected_abbreviation = "tc"
    expected_workspace_id = 123
    expected_evaluation_board_id = 456
    expected_retrospective_board_id = 789

    @patch("crafty_bot.sprint_checklist.signals.monday_client")
    @patch("crafty_bot.sprint_checklist.signals.generate_abbreviation")
    def test_cell_create(self, mock_abbrev_generator, mock_monday_client):
        """
        Test cell model instance creation.

        Create a cell model instance with name and check if the necessary
        signals are called and set the expected parameters. Also, check if the
        Monday.com client is called to create the workspace
        """

        mock_abbrev_generator.return_value = self.expected_abbreviation

        mock_monday_client.create_workspace.return_value = Mock(
            id=self.expected_workspace_id,
        )

        mock_monday_client.create_board_from_template.side_effect = [
            Mock(id=self.expected_evaluation_board_id),
            Mock(id=self.expected_retrospective_board_id),
        ]

        cell = Cell.objects.create(name=self.expected_name)

        assert cell.name == self.expected_name
        assert cell.abbreviation == self.expected_abbreviation
        assert cell.workspace_id == self.expected_workspace_id
        assert str(cell) == f"{self.expected_name} ({self.expected_abbreviation})"

        mock_abbrev_generator.assert_called_once_with(
            sender=Cell,
            name=self.expected_name,
            max_length=6,
        )

        mock_monday_client.create_workspace.assert_called_once_with(
            name=self.expected_name,
        )

        mock_monday_client.create_board_from_template.assert_has_calls(
            [
                call(
                    board_name="Sprint evaluation",
                    template_id=settings.MONDAY_SPRINT_EVALUATION_TEMPLATE_ID,
                    workspace_id=self.expected_workspace_id,
                ),
                call(
                    board_name="Sprint retrospective",
                    template_id=settings.MONDAY_SPRINT_RETROSPECTIVE_TEMPLATE_ID,
                    workspace_id=self.expected_workspace_id,
                ),
            ]
        )

    @patch("crafty_bot.sprint_checklist.signals.monday_client")
    @patch("crafty_bot.sprint_checklist.signals.generate_abbreviation")
    def test_cell_create_workspace_exists(
        self, mock_abbrev_generator, mock_monday_client
    ):
        """
        Test cell model instance creation without creating workspace.

        Create a cell model instance with name and workspace ID, and check if
        the necessary signals are called and set the expected parameters. Ensure
        that the Monday.com client is not called since the workspace ID is
        already set on the model instance.
        """

        mock_abbrev_generator.return_value = self.expected_abbreviation

        mock_monday_client.create_board_from_template.side_effect = [
            Mock(id=self.expected_evaluation_board_id),
            Mock(id=self.expected_retrospective_board_id),
        ]

        cell = Cell.objects.create(
            name=self.expected_name,
            workspace_id=self.expected_workspace_id,
        )

        assert cell.name == self.expected_name
        assert cell.abbreviation == self.expected_abbreviation
        assert cell.workspace_id == self.expected_workspace_id
        assert mock_monday_client.create_workspace.called is False

        mock_abbrev_generator.assert_called_once_with(
            sender=Cell,
            name=self.expected_name,
            max_length=6,
        )

    @patch("crafty_bot.sprint_checklist.signals.monday_client")
    @patch("crafty_bot.sprint_checklist.signals.generate_abbreviation")
    def test_cell_create_abbreviation_set(
        self, mock_abbrev_generator, mock_monday_client
    ):
        """
        Test cell model instance creation with existing abbreviation.

        Create a cell model instance with name and abbreviation, and check if
        the necessary signals are called and set the expected parameters. Check
        if the abbreviation generator is not called if abbreviation is already
        set.
        """

        mock_monday_client.create_workspace.return_value = Mock(
            id=self.expected_workspace_id,
        )

        mock_monday_client.create_board_from_template.side_effect = [
            Mock(id=self.expected_evaluation_board_id),
            Mock(id=self.expected_retrospective_board_id),
        ]

        cell = Cell.objects.create(
            name=self.expected_name,
            abbreviation=self.expected_abbreviation,
        )

        assert cell.name == self.expected_name
        assert cell.abbreviation == self.expected_abbreviation
        assert cell.workspace_id == self.expected_workspace_id
        assert mock_abbrev_generator.called is False

        mock_monday_client.create_workspace.assert_called_once_with(
            name=self.expected_name,
        )


@pytest.mark.django_db
class TestRoleModel:
    """
    Test role model functionalities.
    """

    expected_name = "test role"
    expected_abbreviation = "tr"

    @patch("crafty_bot.sprint_checklist.signals.generate_abbreviation")
    def test_role_create(self, mock_abbrev_generator):
        """
        Test role model instance creation.

        Create a role model instance with name and check if the necessary
        signals are called and set the expected parameters. Also, check if the
        abbreviation is generated for the role.
        """

        mock_abbrev_generator.return_value = self.expected_abbreviation

        role = Role.objects.create(name=self.expected_name)

        assert role.name == self.expected_name
        assert role.abbreviation == self.expected_abbreviation
        assert str(role) == f"{self.expected_name} ({self.expected_abbreviation})"

        mock_abbrev_generator.assert_called_once_with(
            sender=Role,
            name=self.expected_name,
            max_length=6,
        )

    @patch("crafty_bot.sprint_checklist.signals.generate_abbreviation")
    def test_role_create_for_non_lowercase(self, mock_abbrev_generator):
        """
        Test role model instance creation.

        Create a role model instance with name and check if the necessary
        signals are called and set the expected parameters. Also, check if the
        abbreviation is generated for the role.
        """

        role = Role.objects.create(
            name=self.expected_name.upper(),
            abbreviation=self.expected_abbreviation.upper(),
        )

        assert role.name == self.expected_name
        assert role.abbreviation == self.expected_abbreviation
        assert str(role) == f"{self.expected_name} ({self.expected_abbreviation})"
        assert mock_abbrev_generator.called is False

    @patch("crafty_bot.sprint_checklist.signals.generate_abbreviation")
    def test_role_create_abbreviation_set(self, mock_abbrev_generator):
        """
        Test role model instance creation with existing abbreviation.

        Create a role model instance with name and abbreviation, and check if
        the necessary signals are called and set the expected parameters. Check
        if the abbreviation generator is not called if abbreviation is already
        set.
        """

        role = Role.objects.create(
            name=self.expected_name,
            abbreviation=self.expected_abbreviation,
        )

        assert role.name == self.expected_name
        assert role.abbreviation == self.expected_abbreviation
        assert mock_abbrev_generator.called is False


@pytest.mark.django_db
class TestSprintChecklistModel:  # pylint: disable=too-few-public-methods
    """
    Test sprint checklist model functionalities.
    """

    expected_board_name = "test sprint checklist"
    expected_board_state = "active"

    def test_sprint_checklist_create(self):
        """
        Test sprint checklist model instance creation.

        Check if string representation of the model matches the expectations.
        """

        board_str = f"{self.expected_board_name} ({self.expected_board_state})"

        cell = Cell.objects.create(
            name="test cell",
            abbreviation="tc",
            workspace_id=1,
            evaluation_board_id=2,
            retrospective_board_id=3,
        )

        checklist = SprintChecklist.objects.create(
            board_id=1,
            board_name=self.expected_board_name,
            state=self.expected_board_state,
            cell=cell,
            sprint_id=1,
            member="",
        )

        assert str(checklist) == board_str
