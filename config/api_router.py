from typing import List

from django.conf import settings
from django.urls import URLResolver, path
from rest_framework.routers import DefaultRouter, SimpleRouter

from crafty_bot.sprint_checklist.api import CreateSprintChecklistView

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

app_name = "api"

urlpatterns: List[URLResolver] = router.urls
urlpatterns.extend([path("v1/checklist", CreateSprintChecklistView.as_view())])
