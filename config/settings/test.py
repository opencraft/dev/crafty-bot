"""
With these settings, tests run faster.
"""

from .base import *  # noqa
from .base import env

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="Z88AoOQRDPKKuLoGcgBtoEIgdg2XfdfAqIxA66p72pny8WuA8ZBhqmlDyGUZShkQ",
)
# https://docs.djangoproject.com/en/dev/ref/settings/#test-runner
TEST_RUNNER = "config.runner.PytestTestRunner"

# CACHES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "",
    }
}

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

# TEMPLATES
# ------------------------------------------------------------------------------
TEMPLATES[-1]["OPTIONS"]["loaders"] = [  # type: ignore[index] # noqa F405
    (
        "django.template.loaders.cached.Loader",
        [
            "django.template.loaders.filesystem.Loader",
            "django.template.loaders.app_directories.Loader",
        ],
    )
]

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"

# Your stuff...
# ------------------------------------------------------------------------------
MONDAY_API_TOKEN = env.str("MONDAY_API_TOKEN", "fake")
MONDAY_SPRINT_CHECKLIST_TEMPLATE_ID = env.int(
    "MONDAY_SPRINT_CHECKLIST_TEMPLATE_ID", 123456
)
MONDAY_SPRINT_EVALUATION_TEMPLATE_ID = env.int(
    "MONDAY_SPRINT_EVALUATION_TEMPLATE_ID", 123456
)
MONDAY_SPRINT_RETROSPECTIVE_TEMPLATE_ID = env.int(
    "MONDAY_SPRINT_RETROSPECTIVE_TEMPLATE_ID", 123456
)
