# Crafty Bot

[![pipeline status](https://gitlab.com/opencraft/dev/crafty-bot/badges/master/pipeline.svg)](https://gitlab.com/opencraft/dev/crafty-bot/-/commits/master)
[![coverage report](https://gitlab.com/opencraft/dev/crafty-bot/badges/master/coverage.svg)](https://gitlab.com/opencraft/dev/crafty-bot/-/commits/master)

The OpenCraft automation bot.

## Install

The development environment is containerized and running on Docker, hence you
will need to [install Docker](https://www.docker.com/get-started). Beside
Docker, we use [pre-commit](https://pre-commit.com/) for running some quality
tests and enforce formatting before committing.

After `pre-commit` is installed, run the following to setup the git hooks and
download the required plugins.

```sh
pre-commit install
```

As part of the installation, create a new Access Token for your user on
Monday.com. Be aware that the API user can do EVERYTHING your account can, and
we have no sandbox account.

In the `.envs/.local` directory create a `.secret` file and add the following:

```
MONDAY_API_TOKEN=<TOKEN>
```

## Usage

### Start services for development

```sh
make run
```

### Initial database setup

At the beginning, you have a database without applied migrations. Make sure to
run the following commands before the first use to apply the migrations and
create a superuser.

```sh
$ docker-compose run --rm django python manage.py migrate
$ docker-compose run --rm django python manage.py createsuperuser
```

### Start services for production

Production [docker-compose file][file] is shipped with the corresponding ansible
role.

In case you need to start the production environment locally (for any reason),
copy the content of production docker-compose file, create a `.env` file
containing all the necessary production configuration and start the containers.

[file]: https://github.com/open-craft/ansible-playbooks/blob/master/playbooks/roles/crafty-bot/files/docker-compose.yml

## Run code formatting

We use automated code formatting to ensure the same coding style (partially). To
run the automated formatters, you have multiple options.

```sh
make test.pre-commit    ## Will run quality checks and format the code.
make format             ## Run formatters.
```

## Run tests

First, you will need to have a running development environment.

In case of using VSCode for running the tests, you will need to export every
environment variables which are in the local .envs.

```sh
export $(cat .envs/.local/.{django,postgres} | grep -v "#" | xargs)
```

Regardless how you run tests, the first is to export the following two variables
within the container.

```sh
export CELERY_BROKER_URL="${REDIS_URL}"
export DATABASE_URL="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}"
```

### Running test using VSCode

VSCode has a great support for "remote" development using containers or SSH
connection. The project already defines the remote development settings. For
more information how to set up the remote development environment, please refer
to [VSCode's post](https://code.visualstudio.com/blogs/2019/05/02/remote-development).

Within the remote development container, install the dependencies and run the
test command.

```sh
make install_local_dependencies
make test
```

### Running tests in the container

When you are directly using the service container, you won't have to install
requirements, so you can go on with the test command in the first place.

```sh
make test
```

### Running quality tests

We run quality tests - with different setup - in three ways: CI/CD pipelines,
before committing, and manually. To run quality tests manually, you can run
`make test.quality`, though checking pre-commit tests you may want to run
`make test.pre-commit`. For the latter one you will need to
[install](https://pre-commit.com/#install) `pre-commit`.

```sh
make test.migrations     ## Check if migrations are missing.
make test.pre-commit     ## Run pre-commit tests.
make test.quality        ## Run quality tests.
```

## Manual integration testing

It is risky to test changes against [monday.com](https://monday.com) API, since we have no separate account for sandbox. Because of this, please make sure you are carefully executing the following steps when you are doing manual integration testing.

1. Create a monday.com personal access token if you are an admin (You must be an admin, otherwise ask an admin to help you)
2. In the `.envs/.local/.secret` file, create a new variable and set its value to the token: `MONDAY_API_TOKEN=<TOKEN>`
3. Start crafty bot with `docker-compose up` or `make run` as written above in the "Start services for development" section
4. Initialize the database described in the "Initial database setup" step
5. Create a new Cell with some name which is **NOT** used (i.e. **not** serenity, bebop or falcon) like `MyTestCell`
6. \<DO WHATEVER YOU NEED TO DO\>

**Keep in mind:**
* Wait some minutes until event propagation ends on monday.com's side if you sent an API request
* After you did the testing and you don't need the cell anymore, do a cleanup

## Documentation

The project contains two kind of documentation. One for the code using Sphinx
and one for the APIs.

### Sphinx documentation

The Shpinx documentation is generated and served locally on port `7000`. To
access the Sphinx documentation, open [http://localhost:7000](http://localhost:7000).

### API documentation

As a de facto standard we serve Swagger based documentation too with Swagger UI
and ReDoc.

To access Swagger UI, navigate to [http://localhost:8000/swagger](http://localhost:8000/swagger).
In case you prefer ReDoc, visit [http://localhost:8000/redoc](http://localhost:8000/redoc).
Both URLs are using the same [swagger json](http://localhost:8000/swagger.json).
