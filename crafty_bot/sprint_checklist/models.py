"""
Models belonging to sprint checklist app.

Since we are trying to depend on the information what's in Monday.com, only
those models are defined which are strictly need to replay a personalization in
case it failed.

For future reference: The duplicated amout of information should be as few as
possible.
"""
from typing import Union

from django.core.exceptions import ValidationError
from django.db import models


def validate_positive(value: Union[int, float]):
    if value < 0:
        raise ValidationError(f"{value} must be positive")


class Role(models.Model):
    """
    Represents the roles of members in the sprint.

    Roles are controlling what kind of items should remain in the checklist. If
    a role set to "permanent", the related items on the board won't be removed.

    Those members who has roles which are "subscribing to boards", will be added
    to all the boards within a cell per sprint. Hence, in most scenarios, the
    owner and sprint planning managers should have this feature enabled.
    """

    name = models.CharField(
        unique=True, max_length=120, help_text="Full name of the role in lowercase."
    )

    abbreviation = models.CharField(
        unique=True,
        max_length=6,
        blank=True,
        help_text="abbreviation of the role name. If not set, one will be generated.",
    )

    permanent = models.BooleanField(
        default=False, help_text="Indicate if a role must be added to all cell members."
    )

    subscribe_to_boards = models.BooleanField(
        default=False,
        help_text="""Controls if the user with the given role should be subscribed to all
        sprint checklists within the given sprint for the belonging cell.""",
    )

    def __str__(self) -> str:
        """
        Return the string representation of the model.
        """

        return f"{self.name} ({self.abbreviation})"

    class Meta:
        ordering = ["name"]


class Cell(models.Model):
    """
    Represents the cell of members.
    """

    name = models.CharField(
        unique=True, max_length=120, help_text="Full name of the cell."
    )

    abbreviation = models.CharField(
        unique=True,
        max_length=6,
        blank=True,
        help_text="Abbreviation of the cell name.",
    )

    workspace_id = models.BigIntegerField(
        unique=True,
        validators=[validate_positive],
        help_text="The ID of the workspace.",
    )

    evaluation_board_id = models.BigIntegerField(
        unique=True,
        validators=[validate_positive],
        help_text="The ID of the sprint evaluation board.",
    )

    retrospective_board_id = models.BigIntegerField(
        unique=True,
        validators=[validate_positive],
        help_text="The ID of the retrospective board.",
    )

    checklist_template_id = models.BigIntegerField(
        validators=[validate_positive],
        help_text="Id of the checklist template associated with this cell (null for the default template)",
        null=True,
        blank=True,
    )

    def __str__(self) -> str:
        """
        Return the string representation of the model.
        """

        return f"{self.name} ({self.abbreviation})"

    class Meta:
        ordering = ["name"]


class Board(models.Model):
    """
    Represent the minimal information about boards to be able to handle
    archiving and creating new boards.

    As we cannot filter for boards on Monday.com (other than IDs), we need to
    keep the board ID - board status - cell name mapping in DB.
    """

    STATES = (
        ("all", "all"),
        ("active", "active"),
        ("archived", "archived"),
        ("deleted", "deleted"),
    )

    board_id = models.BigIntegerField(
        unique=True,
        validators=[validate_positive],
        help_text="The ID of the board.",
    )

    board_name = models.CharField(
        unique=True,
        max_length=255,
        validators=[validate_positive],
        help_text="Human readable name of the board.",
    )

    state = models.CharField(
        choices=STATES,
        max_length=8,
        validators=[validate_positive],
        help_text="The current state of a board.",
    )

    cell = models.ForeignKey(
        Cell,
        help_text="Name of the OpenCraft cell where the board belongs to.",
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        """
        Return the string representation of the model.
        """

        return f"{self.board_name} ({self.state})"

    class Meta:
        abstract = True


class SprintChecklist(Board):
    """
    Representation of a sprint checklist belonging to a member.
    """

    sprint_id = models.BigIntegerField(
        validators=[validate_positive],
        help_text="The ID of the sprint.",
    )

    member = models.EmailField(help_text="Email address of the OpenCraft member.")
    roles = models.ManyToManyField(Role)

    class Meta:
        ordering = ["-sprint_id"]
        unique_together = ["sprint_id", "cell", "member"]
